/* This is a generated file, don't edit */

#define NUM_APPLETS 170

const char applet_names[] ALIGN1 = ""
"[" "\0"
"[[" "\0"
"ash" "\0"
"awk" "\0"
"base64" "\0"
"basename" "\0"
"bbconfig" "\0"
"blkid" "\0"
"blockdev" "\0"
"bunzip2" "\0"
"bzcat" "\0"
"bzip2" "\0"
"cal" "\0"
"cat" "\0"
"catv" "\0"
"chattr" "\0"
"chgrp" "\0"
"chmod" "\0"
"chown" "\0"
"chroot" "\0"
"clear" "\0"
"cmp" "\0"
"cp" "\0"
"cpio" "\0"
"cut" "\0"
"date" "\0"
"dc" "\0"
"dd" "\0"
"depmod" "\0"
"devmem" "\0"
"df" "\0"
"diff" "\0"
"dirname" "\0"
"dmesg" "\0"
"dos2unix" "\0"
"du" "\0"
"echo" "\0"
"egrep" "\0"
"env" "\0"
"expand" "\0"
"expr" "\0"
"false" "\0"
"fbset" "\0"
"fdisk" "\0"
"fgrep" "\0"
"find" "\0"
"fold" "\0"
"free" "\0"
"freeramdisk" "\0"
"fstrim" "\0"
"fuser" "\0"
"getopt" "\0"
"grep" "\0"
"groups" "\0"
"gunzip" "\0"
"gzip" "\0"
"head" "\0"
"hexdump" "\0"
"id" "\0"
"insmod" "\0"
"install" "\0"
"kill" "\0"
"killall" "\0"
"killall5" "\0"
"less" "\0"
"ln" "\0"
"losetup" "\0"
"ls" "\0"
"lsattr" "\0"
"lsmod" "\0"
"lsof" "\0"
"lspci" "\0"
"lsusb" "\0"
"lzcat" "\0"
"lzop" "\0"
"lzopcat" "\0"
"makedevs" "\0"
"md5sum" "\0"
"mkdir" "\0"
"mkdosfs" "\0"
"mke2fs" "\0"
"mkfifo" "\0"
"mkfs.ext2" "\0"
"mkfs.vfat" "\0"
"mknod" "\0"
"mkswap" "\0"
"mktemp" "\0"
"modinfo" "\0"
"modprobe" "\0"
"more" "\0"
"mount" "\0"
"mountpoint" "\0"
"mv" "\0"
"nanddump" "\0"
"nandwrite" "\0"
"nice" "\0"
"nohup" "\0"
"od" "\0"
"patch" "\0"
"pgrep" "\0"
"pidof" "\0"
"pkill" "\0"
"printenv" "\0"
"printf" "\0"
"ps" "\0"
"pstree" "\0"
"pwd" "\0"
"rdev" "\0"
"readlink" "\0"
"realpath" "\0"
"renice" "\0"
"reset" "\0"
"resize" "\0"
"rev" "\0"
"rm" "\0"
"rmdir" "\0"
"rmmod" "\0"
"run-parts" "\0"
"sed" "\0"
"seq" "\0"
"setconsole" "\0"
"setserial" "\0"
"setsid" "\0"
"sh" "\0"
"sha1sum" "\0"
"sha256sum" "\0"
"sha512sum" "\0"
"sleep" "\0"
"sort" "\0"
"split" "\0"
"stat" "\0"
"strings" "\0"
"stty" "\0"
"swapoff" "\0"
"swapon" "\0"
"sync" "\0"
"sysctl" "\0"
"tac" "\0"
"tail" "\0"
"tar" "\0"
"tee" "\0"
"test" "\0"
"time" "\0"
"top" "\0"
"touch" "\0"
"tr" "\0"
"true" "\0"
"ttysize" "\0"
"tune2fs" "\0"
"umount" "\0"
"uname" "\0"
"unexpand" "\0"
"uniq" "\0"
"unix2dos" "\0"
"unlzma" "\0"
"unlzop" "\0"
"unxz" "\0"
"unzip" "\0"
"uptime" "\0"
"usleep" "\0"
"uudecode" "\0"
"uuencode" "\0"
"watch" "\0"
"wc" "\0"
"which" "\0"
"whoami" "\0"
"xargs" "\0"
"xzcat" "\0"
"yes" "\0"
"zcat" "\0"
;

#ifndef SKIP_applet_main
int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
ash_main,
awk_main,
base64_main,
basename_main,
bbconfig_main,
blkid_main,
blockdev_main,
bunzip2_main,
bunzip2_main,
bzip2_main,
cal_main,
cat_main,
catv_main,
chattr_main,
chgrp_main,
chmod_main,
chown_main,
chroot_main,
clear_main,
cmp_main,
cp_main,
cpio_main,
cut_main,
date_main,
dc_main,
dd_main,
modprobe_main,
devmem_main,
df_main,
diff_main,
dirname_main,
dmesg_main,
dos2unix_main,
du_main,
echo_main,
grep_main,
env_main,
expand_main,
expr_main,
false_main,
fbset_main,
fdisk_main,
grep_main,
find_main,
fold_main,
free_main,
freeramdisk_main,
fstrim_main,
fuser_main,
getopt_main,
grep_main,
id_main,
gunzip_main,
gzip_main,
head_main,
hexdump_main,
id_main,
modprobe_main,
install_main,
kill_main,
kill_main,
kill_main,
less_main,
ln_main,
losetup_main,
ls_main,
lsattr_main,
modprobe_main,
lsof_main,
lspci_main,
lsusb_main,
unlzma_main,
lzop_main,
lzop_main,
makedevs_main,
md5_sha1_sum_main,
mkdir_main,
mkfs_vfat_main,
mkfs_ext2_main,
mkfifo_main,
mkfs_ext2_main,
mkfs_vfat_main,
mknod_main,
mkswap_main,
mktemp_main,
modinfo_main,
modprobe_main,
more_main,
mount_main,
mountpoint_main,
mv_main,
nandwrite_main,
nandwrite_main,
nice_main,
nohup_main,
od_main,
patch_main,
pgrep_main,
pidof_main,
pgrep_main,
printenv_main,
printf_main,
ps_main,
pstree_main,
pwd_main,
rdev_main,
readlink_main,
realpath_main,
renice_main,
reset_main,
resize_main,
rev_main,
rm_main,
rmdir_main,
modprobe_main,
run_parts_main,
sed_main,
seq_main,
setconsole_main,
setserial_main,
setsid_main,
ash_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
sleep_main,
sort_main,
split_main,
stat_main,
strings_main,
stty_main,
swap_on_off_main,
swap_on_off_main,
sync_main,
sysctl_main,
tac_main,
tail_main,
tar_main,
tee_main,
test_main,
time_main,
top_main,
touch_main,
tr_main,
true_main,
ttysize_main,
tune2fs_main,
umount_main,
uname_main,
expand_main,
uniq_main,
dos2unix_main,
unlzma_main,
lzop_main,
unxz_main,
unzip_main,
uptime_main,
usleep_main,
uudecode_main,
uuencode_main,
watch_main,
wc_main,
which_main,
whoami_main,
xargs_main,
unxz_main,
yes_main,
gunzip_main,
};
#endif

const uint16_t applet_nameofs[] ALIGN2 = {
0x0000,
0x0002,
0x0005,
0x0009,
0x000d,
0x0014,
0x001d,
0x0026,
0x002c,
0x0035,
0x003d,
0x0043,
0x0049,
0x004d,
0x0051,
0x0056,
0x005d,
0x0063,
0x0069,
0x006f,
0x0076,
0x007c,
0x0080,
0x0083,
0x0088,
0x008c,
0x0091,
0x0094,
0x0097,
0x009e,
0x00a5,
0x00a8,
0x00ad,
0x00b5,
0x00bb,
0x00c4,
0x00c7,
0x00cc,
0x00d2,
0x00d6,
0x00dd,
0x00e2,
0x00e8,
0x00ee,
0x00f4,
0x00fa,
0x00ff,
0x0104,
0x0109,
0x0115,
0x011c,
0x0122,
0x0129,
0x012e,
0x0135,
0x013c,
0x0141,
0x0146,
0x014e,
0x0151,
0x0158,
0x0160,
0x0165,
0x016d,
0x0176,
0x017b,
0x017e,
0x0186,
0x0189,
0x0190,
0x0196,
0x019b,
0x01a1,
0x01a7,
0x01ad,
0x01b2,
0x01ba,
0x01c3,
0x01ca,
0x01d0,
0x01d8,
0x01df,
0x01e6,
0x01f0,
0x01fa,
0x0200,
0x0207,
0x020e,
0x0216,
0x021f,
0x0224,
0x022a,
0x0235,
0x0238,
0x0241,
0x024b,
0x0250,
0x0256,
0x0259,
0x025f,
0x0265,
0x026b,
0x0271,
0x027a,
0x0281,
0x0284,
0x028b,
0x028f,
0x0294,
0x029d,
0x02a6,
0x02ad,
0x02b3,
0x02ba,
0x02be,
0x02c1,
0x02c7,
0x02cd,
0x02d7,
0x02db,
0x02df,
0x02ea,
0x02f4,
0x02fb,
0x02fe,
0x0306,
0x0310,
0x031a,
0x0320,
0x0325,
0x032b,
0x0330,
0x0338,
0x033d,
0x0345,
0x034c,
0x0351,
0x0358,
0x035c,
0x0361,
0x0365,
0x0369,
0x036e,
0x0373,
0x0377,
0x037d,
0x0380,
0x0385,
0x038d,
0x0395,
0x039c,
0x03a2,
0x03ab,
0x03b0,
0x03b9,
0x03c0,
0x03c7,
0x03cc,
0x03d2,
0x03d9,
0x03e0,
0x03e9,
0x03f2,
0x03f8,
0x03fb,
0x0401,
0x0408,
0x040e,
0x0414,
0x0418,
};


#define MAX_APPLET_NAME_LEN 11
